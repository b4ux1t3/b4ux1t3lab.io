# b4ux1t3.gitlab.io

This is the source code for <https://b4ux1t3.gitlab.io>. My goal for this page is to make it pure HTML and TypeScript, with no runtimes, libraries, or anything like that.

Well, except maybe Bootstrap. 😉
